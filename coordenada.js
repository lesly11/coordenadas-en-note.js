async function main()
{
    let lat;
    const io = require('console-read-write');
    io.write('Ingresa la coordenada de latitud en decimales:');
    lat= await io.ask();

    partedec= lat%1;
    gradolat= lat-partedec;
    aux= partedec*60;
    partedec= aux%1;
    minlat= aux-partedec; 
    aux= partedec*60;
    partedec= aux%1;
    seglat= aux-partedec

    let long;
    io.write("Ingresa la coordenada de longitud en decimales:");
    long= await io.ask();
    partedec= long%1;
    gradolong= long-partedec;
    aux= partedec*60;
    partedec= aux%1;
    minlong= aux-partedec; 
    aux= partedec*60;
    partedec= aux%1;
    seglong= aux-partedec;
    
    console.log("Latitud Grados:"+ gradolat +" Minutos:"+ minlat +" Segundos:"+seglat);
    console.log("Lonngitud Grados:"+ gradolong +" Minutos:"+ minlong +" Segundos:"+seglong);
}
main();